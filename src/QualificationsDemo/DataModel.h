//
//  DataModel.h
//  QualificationsDemo
//
//  Created by Ricardo on 19/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Qualification;

@interface DataModel : NSObject
@property (strong, nonatomic) NSArray *qualifications;
-(void) qualificationsOnSuccess:(void(^)())success onFail:(void(^)(NSError *error))fail;
@end
