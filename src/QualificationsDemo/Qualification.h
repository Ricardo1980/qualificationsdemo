//
//  Qualification.h
//  QualificationsDemo
//
//  Created by Ricardo on 19/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Qualification : NSObject
@property (nonatomic, strong) NSString *qualificationId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *subjects;
@end