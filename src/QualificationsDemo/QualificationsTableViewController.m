//
//  QualificationsTableViewController.m
//  QualificationsDemo
//
//  Created by Ricardo on 19/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "QualificationsTableViewController.h"
#import "DataModel.h"
#import "Qualification.h"
#import "SubjectsTableViewController.h"

@interface QualificationsTableViewController()

@end

@implementation QualificationsTableViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    // http://stackoverflow.com/questions/1369831/eliminate-extra-separators-below-uitableview-in-iphone-sdk
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.dataModel=[DataModel new];
    
    [self.refreshControl beginRefreshing];
    [self loadQualifications];
}

/*
 Pass the selected qualification to the subjects table
 */
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:NSStringFromClass(SubjectsTableViewController.class)]) {
        Qualification *selectedQualification=self.dataModel.qualifications[[self.tableView indexPathForSelectedRow].row];
        SubjectsTableViewController *vc=(SubjectsTableViewController*)segue.destinationViewController;
        vc.selectedQualification=selectedQualification;
    }
}

/*
 Qualifications without subjects should be selected. Show a message and deselect it
 */
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:NSStringFromClass(SubjectsTableViewController.class)]) {
        NSIndexPath *selectedIndexPath=[self.tableView indexPathForSelectedRow];
        Qualification *selectedQualification=self.dataModel.qualifications[selectedIndexPath.row];
        if (selectedQualification.subjects.count==0) {
            NSString *message=[NSString stringWithFormat:@"There are no subjects for %@.", selectedQualification.name];
            UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Information" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [self.tableView deselectRowAtIndexPath:selectedIndexPath animated:YES];
            return NO;
        }
    }
    return YES;
}

- (IBAction)onRefreshTable:(UIRefreshControl *)sender {
    [self loadQualifications];
}

-(void) loadQualifications {
    
    // using self here doesn't create a retain cycle, because the life cycle of the block is a few seconds
    // https://blackpixel.com/writing/2014/03/capturing-myself.html
    
    // request the qualifications to the data model
    [self.dataModel qualificationsOnSuccess:^{
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
        
    } onFail:^(NSError *error) {
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataModel.qualifications.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QualificationCell" forIndexPath:indexPath];
    Qualification *qualification = self.dataModel.qualifications[indexPath.row];
    cell.textLabel.text = qualification.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu subjects", (unsigned long)qualification.subjects.count];
    cell.accessoryType = (qualification.subjects.count == 0) ? UITableViewCellAccessoryNone : UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

@end
