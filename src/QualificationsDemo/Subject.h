//
//  Subject.h
//  QualificationsDemo
//
//  Created by Ricardo on 19/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subject : NSObject
@property (nonatomic, strong) NSString *subjectId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *colour;
@end

