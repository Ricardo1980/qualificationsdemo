//
//  SubjectsTableViewController.m
//  QualificationsDemo
//
//  Created by Ricardo on 19/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "SubjectsTableViewController.h"
#import "Qualification.h"
#import "Subject.h"
#import "HexColors.h"

@implementation SubjectsTableViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    // http://stackoverflow.com/questions/1369831/eliminate-extra-separators-below-uitableview-in-iphone-sdk
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.selectedQualification.subjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubjectCell" forIndexPath:indexPath];
    Subject *subject=self.selectedQualification.subjects[indexPath.row];
    cell.textLabel.text=subject.title;
    
    // use color from subject if available (and transformed properly)
    UIColor *backgroundColor=[UIColor whiteColor];
    if (subject.colour.length>0) {
        UIColor *transformedColor=[UIColor colorWithHexString:subject.colour];
        if (transformedColor) {
            backgroundColor=transformedColor;
        }
    }
    cell.contentView.backgroundColor=backgroundColor;
    
    return cell;
}

@end
