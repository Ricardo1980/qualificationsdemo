//
//  DataModel.m
//  QualificationsDemo
//
//  Created by Ricardo on 19/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "DataModel.h"
#import <Restkit.h>
#import "Qualification.h"
#import "Subject.h"
#import "HexColors.h"

static NSString* const kUrlBase=@"https://api.gojimo.net/api/v4";
static NSString* const kQualificationsPath=@"qualifications";

@interface DataModel()
@property (strong, nonatomic) RKObjectManager *objectManager;
@end

@implementation DataModel

-(id) init {
    
    self=[super init];
    if (self) {
        
        // intialize the data model as empty
        self.qualifications=[NSArray new];
        
        // restkit
        /*
         RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
         RKLogConfigureByName("RestKit", RKLogLevelDefault);
         RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelDefault);
         */
        self.objectManager=[RKObjectManager managerWithBaseURL:[NSURL URLWithString:kUrlBase]];
        
        // subject mapping
        RKObjectMapping *subjectMapping = [RKObjectMapping mappingForClass:Subject.class];
        [subjectMapping addAttributeMappingsFromDictionary:@{@"id": @"subjectId",
                                                             @"title": @"title",
                                                             @"link": @"link",
                                                             @"colour":@"colour"}];
        
        // qualification mapping
        RKObjectMapping *qualificationMapping = [RKObjectMapping mappingForClass:Qualification.class];
        [qualificationMapping addAttributeMappingsFromDictionary:@{@"id": @"qualificationId",
                                                                   @"name": @"name"}];
        [qualificationMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"subjects"
                                                                                    toKeyPath:@"subjects"
                                                                                  withMapping:subjectMapping]];
        
        // qualifications response
        RKResponseDescriptor *qualificationsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:qualificationMapping method:RKRequestMethodGET pathPattern:kQualificationsPath keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
        [self.objectManager addResponseDescriptor:qualificationsResponseDescriptor];
    }
    return self;
}

-(void) qualificationsOnSuccess:(void(^)())success onFail:(void(^)(NSError *error))fail {

    RKObjectRequestOperation *requestOperation = [self.objectManager appropriateObjectRequestOperationWithObject:nil method:RKRequestMethodGET path:kQualificationsPath parameters:nil];
    
    // self will be locked a few seconds, but it's ok, no need to create a weak reference to self in this case
    [requestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        NSArray *qualifications=mappingResult.array;
        self.qualifications=qualifications;
        if (success) success();
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        self.qualifications=[NSArray new];
        if (fail) fail(error);
    }];
    [self.objectManager enqueueObjectRequestOperation:requestOperation];
}

@end
