//
//  QualificationsTableViewController.h
//  QualificationsDemo
//
//  Created by Ricardo on 19/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DataModel;

@interface QualificationsTableViewController : UITableViewController
@property (nonatomic, strong) DataModel *dataModel;
@end
