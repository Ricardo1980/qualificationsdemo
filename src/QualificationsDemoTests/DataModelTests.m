//
//  DataModelTests.m
//  QualificationsDemo
//
//  Created by Ricardo on 19/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "DataModel.h"

static const NSTimeInterval kRequestTimeout=15;

@interface DataModelTests : XCTestCase

@end

@implementation DataModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testNetworkIntegration {
    XCTestExpectation* expectation = [self expectationWithDescription:@"Asking for qualifications"];
    DataModel* dataModel=[DataModel new];
    [dataModel qualificationsOnSuccess:^{
        [expectation fulfill];
        XCTAssert(dataModel.qualifications.count>0, @"There must be qualifications.");
    } onFail:^(NSError *error) {
        [expectation fulfill];
        XCTAssertNil(error, @"There must not be an error object.");
    }];
    [self waitForExpectationsWithTimeout:kRequestTimeout handler:nil];
}

@end
