Notes about the app.

-The app uses cocoapods, so execute 'pod update'

-For networking and data mapping I used RestKit. In my BitBucket account I have samples using NSURLSession and web services though.

-It follows the typical MVC approach. 

-Given the simplicity of the project, in the data model file I added the service requests as well as the data mapping. But, working on complex projects, that should be splited into different files to keep the project clean and maintaneable.

-I used a Storyboard but it's not needed. Working with branches and workmates I strongly recommend the use of nibs (every merge might be a nightmare using Storyboard). Or in the new Xcode 7, we can use storyboard references.

-It has a small sample test using XCTest.

-Using RestKit transformations it's possible to transform the color string to UIColor, time ran out so I couldn't do it. Here is how can be done:
https://github.com/RestKit/RKValueTransformers

-In subjects table view controller, we can pass the selected Qualification as a property or pass the data model that has de selected qualification. In this case I passed directly the selected qualification.

-About the caching policy, I use the default by RestKit, which is do what response headers have.
It can be added Core Data extremely easy using RestKit, just changing RKObjectMapping by RKEntityMapping and creating the model in Core Data (the diagram).

-The app works in different iPhone models, orientations and iPads.

-Several more notes in comments.